$(function () {

  "use strict";

  //===== Sticky

  $(window).on('scroll', function (event) {
    let scroll = $(window).scrollTop();
    if (scroll < 200) {
      $(".navigation").removeClass("sticky");
    } else {
      $(".navigation").addClass("sticky");
    }
  });

//===== Open and close menu

  $(".navbar-toggler").on('click', function () {
    $(this).toggleClass('active');
  });

  $(".navbar-nav a").on('click', function () {
    $(".navbar-toggler").removeClass('active');
  });

//===== Close menu on click outside

  $(document).mouseup(function (e) {
    let container = $(".navigation");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $(".navbar-toggler").removeClass('active');
      $(".navbar-collapse").removeClass('show');
    }
  });


  //===== Wow animation js

  new WOW().init();
  const logoAnimation = anime.timeline({
    autoplay: true,
    delay: 200
  });

  //===== Logo animation

});

//# sourceMappingURL=app.js.map
